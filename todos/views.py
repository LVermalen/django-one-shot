from django.shortcuts import render
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    context_object_name = "todo_list"
    template_name = "todolist/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todolist/detail.html"
    context_object_name = "todo_detail"


class TodoCreateView(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "todolist/new.html"
    context_object_name = "todo_new"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todolist/edit.html"
    context_object_name = "todo_edit"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todolist/delete.html"
    context_object_name = "todo_delete"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todoitem/new.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list_id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todoitem/edit.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list_id])
